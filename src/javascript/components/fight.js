import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination
    } = controls;

    const state = {
      'firstFighter': firstFighter,
      'secondFighter': secondFighter,
      'firstFighterInBlock': false,
      'secondFighterInBlock': false,
      attackerId: null,
      criticalHit: false,
      criticalHitBlockedForFirst: false,
      criticalHitBlockedForSecond: false
    };

    const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
    const secondFighterHealthBar = document.getElementById('right-fighter-indicator');

    const updateHealthBarView = (fighterId, newHealth) => {
      const fighter = fighterId === 'firstFighter' ? firstFighter : secondFighter;
      const fighterHealthBar = fighterId === 'firstFighter' ? firstFighterHealthBar : secondFighterHealthBar;
      const prevHealth = fighter.health;

      fighterHealthBar.style.width = `${newHealth > 0 ? newHealth * 100 / prevHealth : 0}%`;
    };

    const checkBlockedCriticalHit = id => {
      const hitBlockedState = id === 'firstFighter' ? 'criticalHitBlockedForFirst' : 'criticalHitBlockedForSecond';

      return state[hitBlockedState];
    };

    const activateBlockedCriticalHit = id => {
      const BLOCKED_DELAY = 10000;
      const hitBlockedState = id === 'firstFighter' ? 'criticalHitBlockedForFirst' : 'criticalHitBlockedForSecond';
      state[hitBlockedState] = true;

      setTimeout(() => state[hitBlockedState] = false, BLOCKED_DELAY);
    };

    // EVENT LISTENER SINGLE KEYDOWN
    document.addEventListener('keydown', event => {
      const { attackerId } = state;

      if (event.code !== PlayerOneAttack
        && event.code !== PlayerTwoAttack
        && event.code !== PlayerOneBlock
        && event.code !== PlayerTwoBlock
      ) return;

      if (event.code === PlayerOneAttack) {
        if (state['firstFighterInBlock']) return;

        state.attackerId = 'firstFighter';
      }

      if (event.code === PlayerOneBlock) {
        state['firstFighterInBlock'] = true;
        return;
      }

      if (event.code === PlayerTwoAttack) {
        if (state['secondFighterInBlock']) return;

        state.attackerId = 'secondFighter';
      }

      if (event.code === PlayerTwoBlock) {
        state['secondFighterInBlock'] = true;
        return;
      }

      if (attackerId === 'firstFighter' && state['secondFighterInBlock']) {
        state['secondFighterInBlock'] = false;
        return;
      }

      if (attackerId === 'secondFighter' && state['firstFighterInBlock']) {
        state['firstFighterInBlock'] = false;
        return;
      }

      render();
    });

    // EVENT LISTENER MULTIPLE KEYDOWN
    const runCriticalHit = (hitInfo, codes) => {
      let pressed = new Set();

      document.addEventListener('keydown', event => {
        const id = hitInfo.attackerId;

        const attackerFighterInBlock =
          id === 'firstFighter' && state['firstFighterInBlock'] ||
          id === 'secondFighter' && state['secondFighterInBlock'];

        if (attackerFighterInBlock || checkBlockedCriticalHit(id)) return;

        pressed.add(event.code);

        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }

        pressed.clear();

        state.criticalHit = true;
        state.attackerId = id;
        state[hitInfo.defenderInBlock] = false;
        activateBlockedCriticalHit(id);
        render();
      });

      document.addEventListener('keyup', event => {
        pressed.delete(event.code);
      });
    };

    runCriticalHit({
      attackerId: 'firstFighter',
      defenderInBlock: 'secondFighterInBlock'
    }, PlayerOneCriticalHitCombination);

    runCriticalHit({
      attackerId: 'secondFighter',
      defenderInBlock: 'firstFighterInBlock'
    }, PlayerTwoCriticalHitCombination);

    // RENDER
    const render = () => {
      const { attackerId, criticalHit } = state;
      const defenderId = attackerId === 'firstFighter' ? 'secondFighter' : 'firstFighter';
      const attacker = state[attackerId];
      const defender = state[defenderId];
      const damage = criticalHit ? getCriticalDamage(attacker) : getDamage(attacker, defender);
      const newHealth = defender.health - damage;

      state[criticalHit] = false;
      state[defenderId] = {
        ...defender,
        health: newHealth
      };

      const attackerHealth = state[attackerId].health;
      const defenderHealth = state[defenderId].health;
      updateHealthBarView(defenderId, newHealth);

      if (attackerHealth > 0 && defenderHealth > 0) return;

      const winnerId = attackerHealth > defenderHealth ? attackerId : defenderId;

      resolve(state[winnerId]);
    };
  });
}

const getRandomArbitrary = (min, max) => {
  return Math.random() * (max - min) + min;
};

export function getCriticalDamage(attacker) {
  const criticalHitChance = 2;

  return attacker.attack * criticalHitChance;
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  return fighter.attack * getRandomArbitrary(1, 2);
}

export function getBlockPower(fighter) {
  return fighter.defense * getRandomArbitrary(1, 2);
}
